const initialState = {
	str: '',
	error: null
};

const code = '1456';

const reducer = (state = initialState, actions) => {
	if (actions.type === 'ADD') {
		if (state.str.length !== 4) {
			return {...state, str: state.str + actions.payload}
		}
	}

	if (actions.type === 'DELETE') {
		if (state.str.length !== 0) {
			return {...state, str: state.str.slice(0, -1)}
		}
	}

	if (actions.type === 'ENTER') {
		if (state.str === code) {
			return {
				...state,
				str: 'Access Granted',
				error: false
			}
		} else {
			return {
				...state,
				str: 'Access Denied',
				error: true
			}
		}
	}

	return state;
};

export default reducer;