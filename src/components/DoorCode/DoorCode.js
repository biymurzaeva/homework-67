import React from 'react';
import './DoorCode.css';
import {useDispatch, useSelector} from "react-redux";

const DoorCode = () => {
	const dispatch = useDispatch();
	let str = useSelector(state => state['str']);
	const err = useSelector(state => state['error']);
	const add = e => dispatch({type: 'ADD', payload: e});
	const deleteCode = () => dispatch({type: 'DELETE'});
	const enter = () => dispatch({type: 'ENTER'});

	let displayStyle = ['Display'];

	if (err === true) {
		displayStyle.push('ErrorCode');
	}

	if (err === false) {
		displayStyle.push('CurrentCode');
	}

	let stars = '';

	if (str.length <= 4) {
		for (let i = 0; i < str.length; i++) {
			stars += '*';
		}
	}

	return (
		<div className="App">
			<div className={displayStyle.join(' ')}>{stars || str}</div>
			<div className="Buttons">
				<button onClick={e => add(e.currentTarget.innerText)}>7</button>
				<button onClick={e => add(e.currentTarget.innerText)}>8</button>
				<button onClick={e => add(e.currentTarget.innerText)}>9</button>
				<button onClick={e => add(e.currentTarget.innerText)}>4</button>
				<button onClick={e => add(e.currentTarget.innerText)}>5</button>
				<button onClick={e => add(e.currentTarget.innerText)}>6</button>
				<button onClick={e => add(e.currentTarget.innerText)}>1</button>
				<button onClick={e => add(e.currentTarget.innerText)}>2</button>
				<button onClick={e => add(e.currentTarget.innerText)}>3</button>
				<button onClick={deleteCode}>&lsaquo;</button>
				<button onClick={e => add(e.currentTarget.innerText)}>0</button>
				<button onClick={enter}>E</button>
			</div>
		</div>
	);
};

export default DoorCode;